#!/usr/local/bin/python
# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from scipy.stats import norm, zscore
import pymongo as mongo
import datetime
import os.path as path
import os
import json
import codecs
import airbnb as ab
from functools import partial
from pathos.multiprocessing import ProcessingPool as Pool
from bson import json_util
import time
import argparse
import math
import uuid
from scrpproxies import proxy


class AirbnbImport:

    def __init__(self, mongo_url, api_key, client_id, target_dir, location=None, currency='USD',
                 number_of_months=6, id=None, is_forced=False, is_listing_details=False, radius=10000, num_per_page=50):
        proxies = proxy.BonanzaProxy(
            os.environ["PROXY_USER"], os.environ["PROXY_PWD"])
        self.airbnb = ab.Airbnb(api_key, client_id, proxies)

        self.mongo_url = mongo_url
        mongoClient = mongo.MongoClient(mongo_url)
        mongoClient.airbnb.listings.create_index([("loc", mongo.GEOSPHERE)])
        mongoClient.airbnb.listings.create_index(
            [("id", mongo.ASCENDING)], unique=True, dropDups=1)
        mongoClient.airbnb.dates.create_index([("loc", mongo.GEOSPHERE)])
        mongoClient.airbnb.dates.create_index(
            [("id", mongo.ASCENDING), ("date", mongo.ASCENDING)], unique=True, dropDups=1)

        uuid_str = str(id)
        if id == None:
            uuid_str = "{}_{}".format(location[0], location[1])
        path_prefix = uuid.uuid5(uuid.NAMESPACE_DNS, uuid_str)

        self.locations_dir = "{}/{}/locations".format(target_dir, path_prefix)
        self.locations_output_dir = "{}/output".format(
            self.locations_dir)
        self.listing_calendar_dir = "{}/{}/calendars".format(
            target_dir, path_prefix)
        self.listing_calendar_output_dir = "{}/output".format(
            self.listing_calendar_dir)

        if not path.isdir(self.locations_dir):
            os.makedirs(self.locations_dir, 755)
        if not path.isdir(self.locations_output_dir):
            os.makedirs(self.locations_output_dir, 755)
        if not path.isdir(self.listing_calendar_dir):
            os.makedirs(self.listing_calendar_dir, 755)
        if not path.isdir(self.listing_calendar_output_dir):
            os.makedirs(self.listing_calendar_output_dir, 755)

        self.radius = radius
        self.location = location
        self.id = id
        self.currency = currency
        self.number_of_months = number_of_months
        self.is_forced = is_forced
        self.is_listing_details = is_listing_details
        self.num_per_page = num_per_page

    def __calc_scan_points(self, coordinates, radius, radius_split=6,  angle_split=3):
        lat, lng = coordinates
        radius_shift = float(radius / radius_split - 1)

        r_earth = 6371000
        lat, lng = coordinates

        circles = []
        current_angle = 0
        current_radius = radius_shift
        angle_shift = 2*math.pi/angle_split

        for i in range(0, (radius_split - 1)):
            for j in range(0, angle_split):
                current_angle += angle_shift
                new_lat = lat + \
                    (current_radius * math.sin(current_angle) /
                     r_earth) * (180 / math.pi)
                new_lng = lng + (current_radius * math.cos(current_angle) /
                                 r_earth) * (180 / math.pi) / math.cos(new_lat * math.pi/180)
                circles.append(
                    {"lng": new_lng, "lat": new_lat, "radius": radius_shift})
            current_radius += radius_shift
            #angle_split = int(math.ceil(angle_split*2))
            angle_split = int(math.ceil(6 * (i+1)))
            angle_shift = 2*math.pi/angle_split

        return circles

    def __calc_box(self, coordinates, radius):
        lat, lng = coordinates
        r_earth = 6371000
        current_angle = 0
        angle_shift = 2*math.pi/4

        points = []
        for j in range(0, 4):
            new_lat = lat + (radius * math.sin(current_angle) /
                             r_earth) * (180 / math.pi)
            new_lng = lng + (radius * math.cos(current_angle) / r_earth) * \
                (180 / math.pi) / math.cos(new_lat * math.pi/180)
            points.append({"lng": new_lng, "lat": new_lat})
            current_angle += angle_shift

        return [[points[1]['lat'], points[0]['lng']], [points[3]['lat'], points[2]['lng']]]

    def __listing_wrapper(self, args):
        return self.__listing_worker(*args)

    def __listing_worker(self, filename, box, limit, offset, is_listing_details):
        try:
            search_results = self.airbnb.get_listings_for_location(box[0][0], box[0][1], box[1][0],
                                                                   box[1][1], limit, offset, 'en', self.currency)
            with codecs.open(filename, "wb") as fo:
                for search_result in search_results['explore_tabs'][0]["sections"][0]['listings']:
                    listing_id = search_result['listing']['id']
                    try:
                        if is_listing_details:
                            result = self.airbnb.get_listing(listing_id)
                            listing = result['listing']
                        else:
                            listing = search_result['listing']

                        lng = listing['lng']
                        lat = listing['lat']

                        listing['loc'] = {
                            'type': 'Point', 'coordinates': [lng, lat]}

                        del listing['lat']
                        del listing['lng']

                        fo.write(json.dumps(listing) + "\n")
                    except Exception as e:
                        print 'scrape of {} failed with {}'.format(listing_id, e)

        except Exception as e:
            print 'importing listings details failed with {0}\n'.format(e)

    def __calendar_wrapper(self, args):
        return self.__calendar_worker(*args)

    def __calendar_worker(self, listing_filename, listing_id, start_month, start_year, months, locale, currency):
        try:
            calendar = self.airbnb.get_calendar(
                listing_id, start_month, start_year, months, locale, currency)
            with open(listing_filename, 'wb') as listing_f:
                import json
                from bson import json_util
                json.dump(calendar, listing_f, default=json_util.default)
        except Exception as e:
            print "error {0} on listing {1}".format(e, listing_id)

    def do_get_airbnb_listing(self):
        hotels = mongo.MongoClient(self.mongo_url).hotels.hotels

        print 'scraping of airbnb listings...'

        if self.id != None:
            lat, lng = self.__do_import_root_listing(id)
            self.location = [lat, lng]
 #
        scan_locs = self.__calc_scan_points(
            (self.location[0], self.location[1]), self.radius, 4, 3)

        locations = []
        for scan_loc in scan_locs:
            box = self.__calc_box(
                (scan_loc['lat'], scan_loc['lng']), scan_loc['radius'])
            locations.append({'ufi': 0, 'box': box, 'loc': [
                             scan_loc['lat'], scan_loc['lng']], 'radius': scan_loc['radius']})

        locations_df = pd.DataFrame(list(locations))
        for index, row in locations_df.iterrows():
            self.__handle_listings(
                row['loc'], row['box'], self.is_listing_details)

        print 'scraping of airbnb listings succeeded.'

    def __do_import_root_listing(self, id):
        result = self.airbnb.get_listing(id, 'en', self.currency)
        listing = result['listing']
        lng = listing['lng']
        lat = listing['lat']
        filename = '{0}/{1}_{2}_{3}.json'.format(self.locations_output_dir,
                                                 lat,
                                                 lng,
                                                 0)

        if not path.isfile(filename):
            with codecs.open(filename, "wb") as fo:
                listing['loc'] = {
                    'type': 'Point', 'coordinates': [lng, lat]}

                del listing['lat']
                del listing['lng']

                fo.write(json.dumps(listing) + "\n")

        return (lat, lng)

    def __handle_listings(self, location, box, is_listing_details):
        print 'importing listings details for {},{}...'.format(location[0], location[1])

        file_num = 1
        limit = self.num_per_page
        num_files = int(math.ceil(300/limit))

        offset = 0

        params = []
        pool = Pool(20)
        for file_num in range(0, num_files):
            filename = '{0}/{1}_{2}_{3}.json'.format(self.locations_output_dir,
                                                     location[0], location[1], file_num)
            if self.is_forced or not path.isfile(filename):
                params.append((filename, box, limit,
                               offset, is_listing_details))

            offset = offset + limit
            file_num += 1

        start = time.time()
        print "proceeding pool..."
        pool.map(self.__listing_wrapper, params)
        print "pool of {1} pages proceeded in {0}s.".format(time.time() - start, len(params))

        print 'importing listings details suceeded.\n'

    def do_import_listing_files(self):
        print 'importing of airbnb listings files...'
        for filename in os.listdir(self.locations_dir):
            in_file_fullname = '{0}/{1}'.format(self.locations_dir, filename)
            out_file_fullname = '{0}/{1}'.format(
                self.locations_output_dir, filename)

            if self.is_forced or not path.isfile(out_file_fullname):
                if filename.endswith('.json'):
                    with open(in_file_fullname, 'rb') as f:
                        data = json.load(f)

                    with codecs.open(out_file_fullname, "wb") as fo:
                        for result in data['search_results']:
                            location = result['listing']
                            location['pricing'] = result['pricing_quote']
                            location['loc'] = {'type': 'Point', 'coordinates': [
                                location['lng'], location['lat']]}
                            del location['lat']
                            del location['lng']
                            fo.write(json.dumps(location) + "\n")

        print 'importing of airbnb listings files suceeded.'

    def do_import_listing_documents(self):
        print 'importing of airbnb listings into db...'
        listings = mongo.MongoClient(self.mongo_url).airbnb.listings

        for filename in os.listdir(self.locations_output_dir):
            os.system('mongoimport --host mongodb --upsertFields id --db airbnb --quiet --collection listings --file {0}/{1}'.format(
                self.locations_output_dir, filename))

        print 'importing of airbnb listings into db suceeded.'

    def do_import_all(self):
        self.do_listing_import_all()
        self.do_calendar_import_all()

    def do_listing_import_all(self):
        self.do_get_airbnb_listing()
        self.do_import_listing_files()
        self.do_import_listing_documents()

    def do_calendar_import_all(self):
        self.do_get_airbnb_calendar()
        self.do_import_calendar_files()
        self.do_import_calendar_documents()

    def do_get_airbnb_calendar(self):
        params = []

        print 'scraping of airbnb calendars...'
        lst_coll = mongo.MongoClient(self.mongo_url).airbnb.listings

        result = lst_coll.aggregate(
            [
                {
                    '$geoNear':
                    {
                        'near':
                        {
                            'type': 'Point',
                            'coordinates': [self.location[1], self.location[0]]
                        },
                        'spherical': True,
                        'query': {},
                        'maxDistance': self.radius,
                        'distanceField': 'dist',
                        'limit': 100000
                    },
                },
                {
                    '$project':
                    {
                        'id': '$id',
                        'dist': '$dist',
                        'loc': '$loc'
                    }
                }
            ])
        listings = pd.DataFrame(list(result))

        params = []
        now = datetime.datetime.now()
        start_year = now.year
        start_month = now.month

        pool = Pool(20)
        for index, lst in listings.iterrows():
            listing_filename = '{0}/{1}_{2}_{3}.json'.format(
                self.listing_calendar_dir, lst['id'], lst['loc']['coordinates'][1],  lst['loc']['coordinates'][0])
            params.append((listing_filename, lst['id'],
                           start_month, start_year, self.number_of_months, 'en', self.currency))
        start = time.time()
        print "proceeding pool..."
        pool.map(self.__calendar_wrapper, params)
        print "pool of {1} calendars proceeded in {0}s.".format(time.time() - start, len(params))

        print 'scraping of airbnb calendars succeeded.'

    def do_import_calendar_files(self):
        print 'importing of airbnb calendar files...'
        dates = mongo.MongoClient(self.mongo_url).airbnb.dates

        dates.create_index(
            [("id", mongo.ASCENDING), ("date", mongo.ASCENDING)], unique=True)
        dates.create_index([("place_id", mongo.ASCENDING)])
        dates.create_index([("loc", mongo.GEOSPHERE)])

        for filename in os.listdir(self.listing_calendar_dir):
            if filename.endswith('.json'):
                name_strs = filename[:filename.find('.json')].split('_')
                listing_id = int(name_strs[0])
                latitude = float(name_strs[1])
                longitude = float(name_strs[2])

                in_file_fullname = '{0}/{1}'.format(
                    self.listing_calendar_dir, filename)
                out_file_fullname = '{0}/{1}'.format(
                    self.listing_calendar_output_dir, filename)
                if self.is_forced or not path.isfile(out_file_fullname):
                    with open(in_file_fullname) as f:
                        data = json.load(f)
                        with codecs.open(out_file_fullname, "wb") as fo:
                            for month in data['calendar_months']:
                                name = month['name']
                                for day in month['days']:
                                    date = day
                                    date_obj = datetime.datetime.strptime(
                                        day['date'], '%Y-%m-%d')
                                    date['date'] = date_obj
                                    date['id'] = int(listing_id)
                                    date['loc'] = {'type': 'Point', 'coordinates': [
                                        longitude, latitude]}
                                    json.dump(
                                        date, fo, default=json_util.default)

        print 'importing of airbnb calendar files suceeded.'

    def do_import_calendar_documents(self):
        print 'importing of airbnb calendars into db...'

        for filename in os.listdir(self.listing_calendar_output_dir):
            os.system('mongoimport --host mongodb --quiet --upsertFields id,date --db airbnb --collection dates --file {0}/{1}'.format(
                self.listing_calendar_output_dir, filename))

        print 'importing of airbnb calendars into db suceeded.'


def get_method(obj, method_name):
    method = None
    try:
        return getattr(obj, method_name)
    except AttributeError:
        raise NotImplementedError("Class `{}` does not implement `{}`".format(
            obj.__class__.__name__, method_name))


if __name__ == "__main__":
    FUNCTION_MAP = {'calendar_db': 'do_import_calendar_documents', 'calendar_scrape': 'do_get_airbnb_calendar',
                    'calendar_files': 'do_import_calendar_files', 'calendar_all': 'do_calendar_import_all',
                    'listing_scrape': 'do_get_airbnb_listing', 'listing_all': 'do_listing_import_all',
                    'listing_files': 'do_import_listing_files', 'listing_db': 'do_import_listing_documents',
                    'all': 'do_import_all'}
    parser = argparse.ArgumentParser(
        description='Imports Airbnb data.')

    parser.add_argument('command', choices=FUNCTION_MAP.keys())
    parser.add_argument('-l', '--location', type=float, nargs='+')
    parser.add_argument('-i', '--id', type=int)
    parser.add_argument('--force', action='store_true')
    parser.add_argument('--details', action='store_true')
    parser.add_argument('-t', '--target')
    parser.add_argument('-c', '--currency')
    parser.add_argument('-r', '--radius')
    parser.add_argument('-m', '--numberOfMonths')
    parser.add_argument('-db', '--dbUri')

    args = parser.parse_args()

    is_forced = args.force
    is_listing_details = args.details

    radius = 10000
    if args.radius != None:
        radius = int(args.radius)

    currency = 'USD'
    if args.currency != None:
        currency = args.currency

    number_of_months = 6
    if args.numberOfMonths != None:
        number_of_months = int(args.numberOfMonths)

    mongourl = 'mongodb://mongodb'
    if args.dbUri != None:
        mongourl = args.dbUri

    if args.target != None:
        target = args.target
    else:
        target = 'output/airbnb'

    location = None
    if args.location != None:
        if args.target == None:
            raise Exception('Location needs a target folder for export files')

        if len(args.location) == 2:
            location = args.location
        else:
            raise Exception('Only 2 arguments allowed: latitude longitude')

    id = None
    if args.id != None:
        id = args.id

    airbnb_import = AirbnbImport(mongourl, 'd306zoyjsyarp7ifhu67rjxn52tv0t20', '3092nxybyb0otqw18e8nh5nty',
                                 target, location, currency, number_of_months, id, is_forced, is_listing_details)

    method = get_method(airbnb_import, FUNCTION_MAP[args.command])
    method()
