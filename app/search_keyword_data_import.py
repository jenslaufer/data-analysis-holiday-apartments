# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import os.path as path
import os
import pymongo as mongo
import datetime
import json


IMPORT_DIR = 'input/google/'

MONGO_URL = 'mongodb://mongodb'


def to_lowercase(val):
    return val.lower()


def to_int(val):
    return int(val)


def change_key(keyword):
    if 'hotel ' in keyword:
        return keyword.replace('hotel ', '').strip()
    elif 'hotels ' in keyword:
        return keyword.replace('hotels ', '').strip()
    else:
        return keyword.strip()


def remove_chars(city):
    if ' - ' in city:
        return city.replace(' - ', ' ').strip()
    elif '-' in city:
        return city.replace('-', ' ').strip()
    else:
        return city.strip()


def do_import():

    searches_df = pd.DataFrame()

    for filename in os.listdir(IMPORT_DIR):
        search_df = pd.read_csv('{0}/{1}'.format(IMPORT_DIR, filename),
                                sep='\t', error_bad_lines=False, encoding='utf-8')
        searches_df = searches_df.append(search_df, ignore_index=True)

    searches_df = searches_df.drop(['In account?', 'Extracted From', 'Ad group', 'Impr. share', 'Organic impr. share', 'Organic avg. position',
                                    'In plan?'], axis=1)

    searches_df = searches_df.rename(columns={'Avg. Monthly Searches (exact match only)': 'searches', 'Competition': 'competition',
                                              'Suggested bid': 'price', 'Currency': 'currency'})

    searches_df = searches_df.rename(columns={'Keyword': 'keyword'})

    searches_df['city'] = searches_df.keyword

    searches_df['city'] = searches_df.city.apply(
        to_lowercase).apply(remove_chars)
    searches_df.city = searches_df.city.apply(change_key)

    searches_col = mongo.MongoClient(MONGO_URL).search.cityHotelSearches

    records = json.loads(searches_df.T.to_json()).values()

    for record in records:
        searches_col.find_one_and_replace(
            {'city': record['city']}, record, upsert=True)

if __name__ == "__main__":
    do_import()
