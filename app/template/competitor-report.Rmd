---
title: "Occupany and Average Daily Rate Heatmaps"
author: "http://bnbdata.co"
website: "http://bnbdata.co"
url: "http://bnbdata.co"
date: "`r Sys.Date()`"
params:
   mongourl: mongodb://localhost:27017
   currency: CAD
   name: Burma Park on the Bench
   lat: 43.145017
   lng: -79.413085
   radius: 10000
   root.listing.id: 9970665
   number.of.months: 6
output:
  flexdashboard::flex_dashboard:
     theme: readable
---

```{r setup, include=FALSE}
#spacelab, lumen, united, 
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, message = FALSE)
```

```{r}
DB_URL <- params$mongourl
LOCATION_NAME <-  params$name
CURRENCY <- params$currency
```

```{r}
library(tidyverse)
library(jsonlite)
library(mongolite)
library(DT)
library(glue)
```




```{r}
pipeline <-  glue(
'[{{
      "$geoNear":
        {{
            "near":
            {{
                "type": "Point",
                "coordinates": [{params$lng}, {params$lat}]
            }},
            "spherical": 1,
            "maxDistance":  {params$radius},
            "distanceField": "dist",
            "limit": 100000
        }}
    }}]'
)
```




```{r}


listings_col <- mongo("listings", "airbnb", url = DB_URL)
calendar_dates_col <- mongo("dates", "airbnb", url = DB_URL)


listings_df <- flatten(listings_col$aggregate(pipeline))
calendar_df <- flatten(calendar_dates_col$aggregate(pipeline))
```


```{r}

 daily_revenue <- function(available, price) {
    ifelse(available == FALSE, price, 0)
 }

 average_daily_rate <- function(available, revenue) {
   if(length(which(available==FALSE)) > 0){
      revenue / length(which(available==FALSE))
   }
   else{
      0
   }
 }

 occupancy <- function(available) {
    length(which(available==FALSE)) / length(available)
 }
 
 revenue_per_available_room <- function(available, revenue) {
    average_daily_rate(available, revenue) * occupancy(available)
 }
 
 yearly_revenue <- function(available, revenue) {
    365 * revenue_per_available_room(available,revenue)
 }
 

 
# Calculation of metrics and join into the listings dataframe
listings_df <- calendar_df %>% 
          mutate(daily_revenue=daily_revenue(available, price.local_price)) %>% 
          group_by(id) %>% 
          summarize(currency=first(price.local_currency),
                    occupancy=occupancy(available),
                    revenue_per_available_room=revenue_per_available_room(available, sum(daily_revenue)),
                    average_daily_rate=average_daily_rate(available, sum(daily_revenue)),
                    yearly_revenue=yearly_revenue(available, sum(daily_revenue))
                    ) %>%
          right_join(listings_df)  
```



```{r}

 prepare_time_series <- function(date_series_df, date_field){
   date <- as.Date(date_series_df[[ grep(date_field, names(date_series_df))]])
   date_series_df$year <- as.numeric(as.POSIXlt(date)$year+1900)      
   date_series_df$month <- as.numeric(as.POSIXlt(date)$mon+1) 
   date_series_df$day <- as.numeric(as.POSIXlt(date)$mday) 
   date_series_df$week <- as.numeric(format(date,"%W"))
   date_series_df$weekday <- as.numeric(as.POSIXlt(date)$wday)
   date_series_df$monthf <- factor(date_series_df$month,levels=as.character(1:12),
                                labels=c("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"),
                                ordered=TRUE) 
   date_series_df$weekdayf <- factor(date_series_df$weekday,levels=rev(0:6),
                                   labels=rev(c("Su", "Mo","Tu","We","Th","Fr","Sa")),ordered=TRUE)
   
   date_series_df <- date_series_df %>% filter(!is.na(week)) %>% group_by_(.dots=c('year', 'month')) %>%             
                     mutate(monthweek= 1 + week - min(week))
   date_series_df$monthweek <- as.factor(date_series_df$monthweek)
   date_series_df
}




```

```{r}
listing_names <- listings_df %>%
  select(id, name)
calendar_df$price.local_price <- as.Date(calendar_df$price.date)
calendar_df <- prepare_time_series(calendar_df, 'price.date') %>%
      inner_join(listing_names)  
```


```{r, fig.height=50, fig.width=20}
p1 <- calendar_df %>%
  arrange(dist) %>%
 ggplot(aes(x = weekdayf, y = monthweek)) + 
    geom_tile(mapping=aes(fill = available)) +
    geom_text(aes(label=day)) +
    scale_y_discrete(limits=rev(levels(calendar_df$monthweek)), labels=c('','','','','','')) +
    scale_x_discrete(limits=c('Mo','Tu','We','Th', 'Fr','Sa','Su')) +  
    labs(x='Day Of Week', y='', fill='available', title='Competitor\'s calendars') +
    facet_grid(name~year~monthf) +
    theme(line = element_blank(), strip.text.y = element_text(angle = 0)) 
ggsave("calendars.png", limitsize = FALSE, width=20, height = 50)
p1
    
```


```{r}
listings_df %>%
  select(id, name, dist, person_capacity, bedrooms, bathrooms,  instant_book_enabled, star_rating,  review_rating_cleanliness, review_rating_location, property_type, room_type, room_type_category, security_price_native, price_for_extra_person_native, cleaning_fee_native, price_native, min_nights, cancel_policy_short_str, reviews_count) %>%
  write_delim(path='competitors.csv', delim = ';')
```

```{r}

```

