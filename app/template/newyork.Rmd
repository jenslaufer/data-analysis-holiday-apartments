---
title: "Market Analysis New York"
params:
   mongourl: mongodb://localhost:27017
   currency: EUR
   name: Holzstrasse Fürth
   lat: 40.66736
   lng: -73.98773 
   radius: 300000
   root.listing.id: 11753760
   number.of.months: 6
   query: "{}"
output: 
  html_document: default
---
   
   
```{r setup, include=FALSE}
#spacelab, lumen, united, 
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, message = FALSE)
```

```{r}
DB_URL <- params$mongourl
LOCATION_NAME <-  params$name
CURRENCY <- params$currency
LATITUDE <- params$lat
LONGITUDE <- params$lng
RADIUS <- params$radius
ROOT_LISTING_ID <- params$root.listing.id
N_COMPETITORS <-  15
QUERY <- params$query

```

```{r}
library(DT)
library(gridExtra)
library(ggthemes)
library(ggrepel)
library(leaflet)
library(scales)
library(ggmap)
library(tidyverse)
```

```{r}
source('include/get_listings_by_location.R')
source('include/heatmap.R')
```


## Are there regional revenue differences


- Facet with apartment size
- Revenue percentiles for buckets




```{r}
listings.with.revenue <- listings_df %>% filter(!is.na(yearly_revenue))  %>%
  mutate(revenue_bucket=cut(yearly_revenue, include.lowest = T,  breaks=c(quantile(yearly_revenue, seq(0,1,0.5)))),
         occupancy_bucket=cut(occupancy*100, include.lowest = T, breaks=c(quantile(occupancy*100, seq(0,1,0.5)))))
```


```{r}
listings.with.revenue %>% select(revenue_bucket, occupancy_bucket)
```




```{r, fig.width=50, fig.height=50}
ggmap(get_map(make_bbox(listings.with.revenue$lng, listings.with.revenue$lat))) +
  geom_point(data=listings.with.revenue,aes(lng, lat, color=revenue_bucket), size=5) +
  scale_color_tableau()+
  facet_wrap(~person_capacity)
  
```




```{r, fig.width=50, fig.height=50}
ggmap(get_map(make_bbox(listings.with.revenue$lng, listings_df$lat))) +
  geom_point(data=listings.with.revenue,aes(lng, lat, color=occupancy_bucket), size=10) +
  scale_color_tableau()+
  facet_wrap(~person_capacity)
  
```