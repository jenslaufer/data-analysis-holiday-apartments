---
title: "Market Analysis New York"
params:
   mongourl: mongodb://localhost:27017
   currency: EUR
   name: Holzstrasse Fürth
   lat: 40.66736
   lng: -73.98773 
   radius: 300000
   root.listing.id: 11753760
   number.of.months: 6
   query: "{}"
output: 
  html_document: default
---
   
   
```{r setup, include=FALSE}
#spacelab, lumen, united, 
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, message = FALSE)
```

```{r}
DB_URL <- params$mongourl
LOCATION_NAME <-  params$name
CURRENCY <- params$currency
LATITUDE <- params$lat
LONGITUDE <- params$lng
RADIUS <- params$radius
ROOT_LISTING_ID <- params$root.listing.id
N_COMPETITORS <-  15
QUERY <- params$query

```

```{r}
library(DT)
library(gridExtra)
library(ggthemes)
library(ggrepel)
library(leaflet)
library(scales)
library(ggmap)
library(tidyverse)
library(leaflet)
library(sf)
```

```{r}
source('include/get_listings_by_location.R')
source('include/heatmap.R')
```


```{r}
available.listings <- calendar_df %>% ungroup() %>% mutate(simple_date=as.Date(date)) %>%  filter(available == T & (simple_date == as.Date('2019-05-22') |
                                                                           simple_date == as.Date('2019-05-23') |
                                                                           simple_date == as.Date('2019-05-24') |
                                                                           simple_date == as.Date('2019-05-25') |
                                                                           simple_date == as.Date('2019-05-26'))) %>% 
                                                                           select(id, date, simple_date, available, price.local_price) %>% 
                                                                           group_by(id, available, price.local_price) %>%
                                                                           summarise(num=sum(available), 
                                                                                     total=sum(price.local_price)) %>%
                                                                           filter(num==5) %>% arrange(total)

available_listings_df <- listings_df%>% right_join(available.listings, by="id") 
```

```{r}

```


```{r}
available_listings_df <- available_listings_df %>%  mutate(person_capacity=as.numeric(person_capacity)) %>% filter(person_capacity >= 2 & room_type=="Entire home/apt") %>% 
   mutate(no=row_number()) %>% filter(neighborhood=='Manhattan')
```





```{r fig.height=10, fig.width=20}
content <- paste('<style>div.leaflet-popup-content {width:auto !important} div.leaflet-popup-content-wrapper {opacity:1}</style>',
                    '<img src="', available_listings_df$picture_url, '"><br>',
                    '<a target="_blank" href="https://airbnb.co.uk/rooms/', available_listings_df$id, '?currency=', CURRENCY,'">', available_listings_df$name, '</a><br><br><br>',
                    "Price: $", available_listings_df$total, "<br>",
                     sep="")
available_listings_df %>% head(30) %>%
  leaflet() %>%
  addTiles(urlTemplate = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png') %>% 
  addCircleMarkers(~lng, ~lat, radius=8,  stroke = FALSE,  labelOptions = labelOptions(noHide = F, textOnly = TRUE,  
                                                                               ,
      style = list(
        "font-style" = "bold",
        "font-size" = "12px"
      )),
      popup = content,
      fillOpacity = 1) 
```
`

