---
title: ''
params:
   mongourl: mongodb://localhost:27017
   currency: CAD
   name: Burma Park on the Bench
   lat: 43.145017
   lng: -79.413085
   radius: 10000
   root.listing.id: 9970665
   number.of.months: 6
output:
  html_document: default
---
   
   
```{r setup, include=FALSE}
#spacelab, lumen, united, 
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, message = FALSE)
```

```{r}
DB_URL <- params$mongourl
LOCATION_NAME <-  params$name
CURRENCY <- params$currency
LATITUDE <- params$lat
LONGITUDE <- params$lng
RADIUS <- params$radius

```

```{r}
library(DT)
library(gridExtra)
```

```{r}
source('include/get_listings_by_location.R')
source('include/heatmap.R')
```


```{r message=FALSE, warning=FALSE, echo=FALSE, settings}
theme_set(theme_bw())
```

```{r}
listings_df$star_rating = as.factor(listings_df$star_rating)

listings_df$reviews_count_bucket <- cut(listings_df$reviews_count, 
                                        breaks =c(0,20,40,60,10000), 
                                        labels = c('-20','20-40','40-60','60+')) 
listings_df$picture_count_bucket <- cut(listings_df$picture_count, 
                                        breaks =c(0,10,20,30,40,50,10000), 
                                        labels = c('-10','10-20','20-30','30-40','40-50','50+'))
listings_df$adr_bucket <- cut(listings_df$average_daily_rate, 
                                        breaks =c(0,40,60,80,100,120,140,180,220,300,400,500,1000000), 
                                        labels = 
                                c('-40','40-60','60-80','80-100',
                                  '100-120','120-140','140-180','180-220','220-300','300-400','400-500','500+')) 
listings_df$price_category = cut(listings_df$average_daily_rate, 
                                        breaks =c(0,20,60,120,200,100000), 
                                        labels = 
                                c('cheap','budget','midclass','upper','luxury')) 
```

# Airbnb market analysis for `r LOCATION_NAME`


## Yearly revenue


```{r echo=FALSE, message=FALSE, warning=FALSE}

median_yearly_revenue <- round(median(listings_df$yearly_revenue,na.rm = TRUE), digits=0)
upper_yearly_revenue <- round(quantile(listings_df$yearly_revenue, probs=c(0.75), na.rm = TRUE), digits=0)
lower_yearly_revenue <- round(quantile(listings_df$yearly_revenue, probs=c(0.25), na.rm = TRUE), digits=0)
without_ouliers_yearly_revenue <- round(quantile(listings_df$yearly_revenue, probs=c(0.01, 0.99), na.rm = TRUE), digits=0)
```


The median for yearly revenue: `r CURRENCY` `r as.integer(median_yearly_revenue)`

50% of all listings have a yearly revenue between 
`r CURRENCY` `r as.integer(lower_yearly_revenue)` and `r CURRENCY` `r as.integer(upper_yearly_revenue)`



```{r echo=FALSE, message=FALSE, warning=FALSE}


p1 <- ggplot(data=listings_df, mapping=aes(x=yearly_revenue)) +
  geom_histogram(alpha=0.3, fill='blue', color='black') +
  scale_x_log10(breaks=c(100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000), 
                labels=c('0.1k','0.2k','0.5k','1k','2k','5k','10k','20k','50k','100k','200k','500k','1M')) +
  labs(x='Yearly revenue', y='Number of listings', fill='Room Type', title='Distribution of yearly revenue of Airbnb listings')

p2 <- ggplot(data=listings_df, mapping=aes(y=yearly_revenue,x=as.factor(''))) +
             geom_boxplot(alpha=0.3, fill= 'blue') +  
             scale_y_log10(breaks=c(100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000), 
                labels=c('0.1k','0.2k','0.5k','1k','2k','5k','10k','20k','50k','100k','200k','500k','1M')) +
             labs(x='', y='Yearly revenue')
             
#ggsave(file="yearly_revenue.png", grid.arrange(p1, p2, nrow=2))

grid.arrange(p1, p2, nrow=2)
```


### Yearly revenue by Average Daily Rate


```{r echo=FALSE, message=FALSE, warning=FALSE}

yr_adr_pl <- ggplot(data=listings_df, mapping=aes(y=yearly_revenue, x=adr_bucket)) +
              geom_boxplot(alpha=0.3, mapping=aes(fill= adr_bucket)) +   
              scale_y_log10(breaks=c(100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000), 
                labels=c('0.1k','0.2k','0.5k','1k','2k','5k','10k','20k','50k','100k','200k', '500k', '1M')) +
              labs(x='Average Daily Rate', fill='Average Daily Rate', y='Yearly revenue')
             
#ggsave(file="yearly_revenue_by_adr.png", yr_adr_pl)
yr_adr_pl
```



### Yearly revenue by person capacity


```{r echo=FALSE, message=FALSE, warning=FALSE}

yr_pc_pl <- ggplot(data=listings_df, mapping=aes(y=yearly_revenue, x=as.factor(person_capacity))) +
              geom_boxplot(alpha=0.3, mapping=aes(fill= as.factor(person_capacity))) +  
              scale_y_log10(breaks=c(100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000), 
                labels=c('0.1k','0.2k','0.5k','1k','2k','5k','10k','20k','50k','100k','200k','500k','1M')) +
              labs(x='Person Capacity', fill='Person Capacity',y='Yearly revenue')
             
#ggsave(file="yearly_revenue_per_person_capacity.png", yr_pc_pl)
yr_pc_pl
```


### Yearly revenue by room type


```{r echo=FALSE, message=FALSE, warning=FALSE}

yr_rt_pl <- ggplot(data=listings_df, mapping=aes(y=yearly_revenue, x=room_type)) +
              geom_boxplot(alpha=0.3, mapping=aes(fill= room_type)) +   
              scale_y_log10(breaks=c(100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000), 
                labels=c('0.1k','0.2k','0.5k','1k','2k','5k','10k','20k','50k','100k','200k','500k','1M')) +
              labs(x='Room Type', fill='Room Type', y='Yearly revenue')
             
#ggsave(file="yearly_revenue_by_room_type.png", yr_rt_pl)
yr_rt_pl
```

### Yearly revenue by price category


```{r echo=FALSE, message=FALSE, warning=FALSE}
yr_pc_pl <- ggplot(data=listings_df, mapping=aes(y=yearly_revenue, x=price_category)) +
              geom_boxplot(alpha=0.3, mapping=aes(fill= price_category)) +   
              scale_y_log10(breaks=c(100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000), 
                labels=c('0.1k','0.2k','0.5k','1k','2k','5k','10k','20k','50k','100k','200k','500k','1M')) +
              labs(x='Price category', fill='Price category', y='Yearly revenue')
             
#ggsave(file="yearly_revenue_by_price_category.png", yr_pc_pl)
yr_pc_pl
```



### Yearly revenue by Rating


```{r echo=FALSE, message=FALSE, warning=FALSE}

yr_sr_pl <- ggplot(data=listings_df, mapping=aes(y=yearly_revenue, x=star_rating)) +
              geom_boxplot(alpha=0.3, mapping=aes(fill= star_rating)) +   
              scale_y_log10(breaks=c(100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000), 
                labels=c('0.1k','0.2k','0.5k','1k','2k','5k','10k','20k','50k','100k','200k','500k','1M')) +
              labs(x='Star rating', fill='Star rating', y='Yearly revenue')
             
#ggsave(file="yearly_revenue_by_star_rating.png", yr_sr_pl)
yr_sr_pl
```


### Yearly revenue by Reviews Count



```{r echo=FALSE, message=FALSE, warning=FALSE}

yr_rc_pl <- ggplot(data=listings_df, mapping=aes(y=yearly_revenue, x=reviews_count_bucket)) +
              geom_boxplot(alpha=0.3, mapping=aes(fill= reviews_count_bucket)) +   
             scale_y_log10(breaks=c(100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000), 
                labels=c('0.1k','0.2k','0.5k','1k','2k','5k','10k','20k','50k','100k','200k','500k','1M')) +
              labs(x='Reviews Count', fill='Reviews Count', y='Yearly revenue')
             
#ggsave(file="yearly_revenue_by_revenue_count.png", yr_rc_pl)
yr_rc_pl
```


### Yearly revenue by Picture Count


```{r echo=FALSE, message=FALSE, warning=FALSE}

yr_pc_pl <- ggplot(data=listings_df, mapping=aes(y=yearly_revenue, x=picture_count_bucket)) +
              geom_boxplot(alpha=0.3, mapping=aes(fill= picture_count_bucket)) +   
              scale_y_log10(breaks=c(100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000), 
                labels=c('0.1k','0.2k','0.5k','1k','2k','5k','10k','20k','50k','100k','200k','500k','1M')) +
              labs(x='Picture Count', fill='Picture Count', y='Yearly revenue')
             
#ggsave(file="yearly_revenue_by_picture_count.png", yr_pc_pl)
yr_pc_pl
```

## Occupancy


```{r echo=FALSE, message=FALSE, warning=FALSE}

median_occupancy <- round(median(listings_df$occupancy,na.rm = TRUE)*100, digits=1)

upper_occupancy <- round(quantile(listings_df$occupancy, probs=c(0.75), na.rm = TRUE) * 100, digits=1)
lower_occupancy <- round(quantile(listings_df$occupancy, probs=c(0.25), na.rm = TRUE) * 100, digits=1)

without_ouliers_occupancy <- round(quantile(listings_df$occupancy, probs=c(0.01, 0.99), na.rm = TRUE) * 100, digits=1)

```


The median for occupancy: `r as.integer(median_occupancy)`%

50% of all listings have a occupancy between 
`r as.integer(lower_occupancy)`% and `r as.integer(upper_occupancy)`%


```{r echo=FALSE, message=FALSE, warning=FALSE}


p3 <- ggplot(data=listings_df, mapping=aes(x=occupancy * 100)) +
        geom_histogram(alpha=0.3, fill='red', color='black') +
        scale_x_continuous(breaks=seq(0,100,10)) +
        coord_cartesian(xlim=without_ouliers_occupancy) +
        labs( x='Occupancy in %', y='Number of listings', title='Occupancy of Airbnb listings')

p4 <- ggplot(data=listings_df, mapping=aes(y=occupancy * 100, x=as.factor(''))) +
        geom_boxplot(alpha=0.3, fill='red') +
        scale_y_continuous(breaks=seq(0,100,10)) +
        coord_cartesian(ylim=c(0, upper_occupancy * 1.2)) +
        labs(y='Occupancy in %', x='')
             
#ggsave(file="occupancy.png", grid.arrange(p3, p4, nrow=2))

grid.arrange(p3, p4, nrow=2)
```

### Occupancy by Average Daily Rate


```{r echo=FALSE, message=FALSE, warning=FALSE}

occ_adr_pl <- ggplot(data=listings_df, mapping=aes(y=occupancy * 100, x=adr_bucket)) +
              geom_boxplot(alpha=0.3, mapping=aes(fill= adr_bucket)) + 
              labs(x='Average Daily Rate', fill='Average Daily Rate', y='Occupancy in %')
             
#ggsave(file="occ_by_adr.png", occ_adr_pl)
occ_adr_pl
```


### Occupancy by person capacity


```{r echo=FALSE, message=FALSE, warning=FALSE}

occ_pc_pl <- ggplot(data=listings_df, mapping=aes(y=occupancy * 100, x=as.factor(person_capacity))) +
              geom_boxplot(alpha=0.3, mapping=aes(fill= as.factor(person_capacity))) + 
              labs(x='Person Capacity', fill='Person Capacity',y='Occupancy in %')
             
#ggsave(file="occupancy_per_person_capacity.png", occ_pc_pl)
occ_pc_pl
```



### Occupancy by Room Type


```{r echo=FALSE, message=FALSE, warning=FALSE}

occ_rt_pl <- ggplot(data=listings_df, mapping=aes(y=occupancy * 100, x=room_type)) +
              geom_boxplot(alpha=0.3, mapping=aes(fill= room_type)) + 
              labs(x='Room type', fill='Room Type',y='Occupancy in %')
             
#ggsave(file="occupancy_per_room_type.png", occ_rt_pl)
occ_rt_pl
```




### Occupancy by Star Rating


```{r echo=FALSE, message=FALSE, warning=FALSE}

occ_sr_pl <- ggplot(data=listings_df, mapping=aes(y=occupancy * 100, x=star_rating)) +
              geom_boxplot(alpha=0.3, mapping=aes(fill= star_rating)) + 
              labs(x='Star rating', fill='Star rating',y='Occupancy in %')
             
#ggsave(file="occupancy_per_star_rating.png", occ_sr_pl)
occ_sr_pl
```


### Occupancy by Reviews Count



```{r echo=FALSE, message=FALSE, warning=FALSE}

occ_rc_pl <- ggplot(data=listings_df, mapping=aes(y=occupancy * 100, x=reviews_count_bucket)) +
              geom_boxplot(alpha=0.3, mapping=aes(fill= reviews_count_bucket)) +   
              labs(x='Reviews Count', fill='Reviews Count', y='Occupancy in %')
             
#ggsave(file="occupancy_by_revenue_count.png", occ_rc_pl)
occ_rc_pl
```



### Occupancy by Picture Count



```{r echo=FALSE, message=FALSE, warning=FALSE}

occ_pc_pl <- ggplot(data=listings_df, mapping=aes(y=occupancy * 100, x=picture_count_bucket)) +
              geom_boxplot(alpha=0.3, mapping=aes(fill= picture_count_bucket)) +   
              labs(x='Pciture Count', fill='Pciture Count', y='Occupancy in %')
             
#ggsave(file="occupancy_by_picture_count.png", occ_pc_pl)
occ_pc_pl
```


## Average daily rate (ADR)


```{r echo=FALSE, message=FALSE, warning=FALSE}

median_adr <- round(median(listings_df$average_daily_rate,na.rm = TRUE), digits=1)
upper_adr <- round(quantile(listings_df$average_daily_rate, probs=c(0.75), na.rm = TRUE), digits=1)
lower_adr <- round(quantile(listings_df$average_daily_rate, probs=c(0.25), na.rm = TRUE), digits=1)

without_ouliers_adr <- round(quantile(listings_df$average_daily_rate, probs=c(0.01,0.99), na.rm = TRUE), digits=1)
```


The median for Average Daily Rate (ADR): `r CURRENCY` `r as.integer(median_adr)`

50% of all listings have a Average Daily Rate (ADR) between 
`r CURRENCY` `r as.integer(lower_adr)` and `r CURRENCY` `r as.integer(upper_adr)`


```{r echo=FALSE, message=FALSE, warning=FALSE}


p5 <- ggplot(data=listings_df, mapping=aes(x=average_daily_rate)) +
        geom_histogram(alpha=0.3, fill='green', color='black', binwidth = 10) +
        coord_cartesian(xlim=without_ouliers_adr) +
        labs(x=paste('Average Daily Rate(ADR) in', CURRENCY), y='Number of listings', title='Average Daily Rate(ADR) of Airbnb listings')

p6 <- ggplot(data=listings_df, mapping=aes(y=average_daily_rate, x=as.factor(''))) +
        geom_boxplot(alpha=0.3, fill='green') +
        coord_cartesian(ylim=c(0, upper_adr * 1.2)) +
        labs( y=paste('Average Daily Rate(ADR) in', CURRENCY), x='')
             
#ggsave(file="adr.png", grid.arrange(p5, p6, nrow=2))

grid.arrange(p5, p6, nrow=2)
```


### Average Daily rate by room type


```{r echo=FALSE, message=FALSE, warning=FALSE}

adr_rt_pl <- ggplot(data=listings_df, mapping=aes(y=average_daily_rate, x=room_type)) +
              geom_boxplot(alpha=0.3, mapping=aes(fill= room_type)) + 
              scale_y_continuous() +
              labs(x='Room Type', fill='Room Type', y='Average Daily Rate')
             
#ggsave(file="adr_by_room_type.png", adr_rt_pl)
adr_rt_pl
```



```{r echo=FALSE, message=FALSE, warning=FALSE}

adr_pc_pl <- ggplot(data=listings_df, mapping=aes(y=average_daily_rate, x=as.factor(person_capacity))) +
              geom_boxplot(alpha=0.3, mapping=aes(fill= as.factor(person_capacity))) + 
              scale_y_continuous() +
              labs(x='Person Capacity', fill='Person Capacity', y='Average Daily Rate')
             
#ggsave(file="adr_by_person_capacity.png", adr_pc_pl)
adr_pc_pl
```

```{r echo=FALSE, message=FALSE, warning=FALSE}
performers <-  function(price_class, add_bottom=FALSE){
   top <- listings_df %>% 
                             filter(price_category==price_class) %>%
                             arrange(desc(yearly_revenue)) %>% 
                             top_n(n=15, wt=yearly_revenue)
   bottom <- listings_df %>% 
                             filter(price_category == price_class) %>%
                             filter(yearly_revenue > 0) %>%
                             arrange(desc(yearly_revenue)) %>% 
                             top_n(n=-15, wt=yearly_revenue)
   if(add_bottom == TRUE){
      performers <- union(top, bottom) %>%
                 arrange(desc(yearly_revenue))
   } else{
      performers = top
   }
   
   performers$occupancy = round(performers$occupancy*100, digits = 1)
   performers$average_daily_rate = round(performers$average_daily_rate,
                                                       digits = 0)
   performers$yearly_revenue = round(performers$yearly_revenue, 
                                                    digits = 0)
   performers
}

performers_table <- function(listings){
  if(nrow(listings) > 0){
     print_df <- listings[ , +which(names(listings_df) %in% c("id", "name", 
                                                           'yearly_revenue', 
                                                           'occupancy',
                                                           'average_daily_rate'))]
     print_df <- print_df[,c(1,5,4,2,3)]
     
     colnames(print_df) <- c('Id', 'Name', 'Yearly Revenue', 'Occupancy', 'Average Daily Rate')
     grid.table(print_df, rows = NULL)
     
     colnames(print_df) <- c('Id', 'Name', 'Yearly Revenue', 'Occupancy', 'Average Daily Rate')
     grid.table(print_df, rows = NULL)
  } else {
     print_df = c()
  }
  
  
  
}

performers_plot <- function(listings, price_category){
   filename <- paste('top_performing_', price_category, '.png', sep='')
   title <- paste("Top performing listings in", price_category, 
                  "price category in", LOCATION_NAME, sep = ' ')
   p1 <- ggplot(data=listings, mapping=aes(x=average_daily_rate,y=yearly_revenue)) +
         geom_point(aes(size=revenue_per_available_room,fill=person_capacity), shape=21)+
         scale_size_continuous(range = c(1, 15)) +
         scale_fill_gradientn(colours = terrain.colors(20)) +
         geom_text(mapping=aes(x=average_daily_rate, y=yearly_revenue, label=name)) + 
         labs(x='Average Daily Rate (ADR)', y=paste('Yearly Revenue in', CURRENCY), fill='Person Capacity', 
              size='Revenue per available room (RevPar)', title=title)
   #ggsave(file=filename, p1)
   p1
}
```


# Top performing listings


### Cheap price category
```{r echo=FALSE, message=FALSE, warning=FALSE}
price_category <-  'cheap'
```



```{r echo=FALSE, message=FALSE, warning=FALSE}

performing_listings <- performers(price_category)
```


```{r echo=FALSE, message=FALSE, warning=FALSE, fig.width=15, fig.height=10}
performers_table(performing_listings)
```



```{r echo=FALSE, message=FALSE, warning=FALSE, fig.width=12, fig.height=9}
performers_plot(performing_listings, price_category)
      
```


### Budget price category
```{r echo=FALSE, message=FALSE, warning=FALSE}

price_category <-  'budget'
```


```{r echo=FALSE, message=FALSE, warning=FALSE}

performing_listings <- performers(price_category)
```



```{r echo=FALSE, message=FALSE, warning=FALSE, fig.width=15, fig.height=10}
performers_table(performing_listings)
```



```{r echo=FALSE, message=FALSE, warning=FALSE, fig.width=15, fig.height=10}
performers_plot(performing_listings, price_category)
      
```



### Midclass price category
```{r echo=FALSE, message=FALSE, warning=FALSE}

price_category <-  'midclass'
```


```{r echo=FALSE, message=FALSE, warning=FALSE}
performing_listings <- performers(price_category)
```



```{r echo=FALSE, message=FALSE, warning=FALSE, fig.width=15, fig.height=10}
performers_table(performing_listings)
```



```{r echo=FALSE, message=FALSE, warning=FALSE, fig.width=15, fig.height=10}
performers_plot(performing_listings, price_category)
      
```


### Upper price category
```{r echo=FALSE, message=FALSE, warning=FALSE}

price_category <-  'upper'
```


```{r echo=FALSE, message=FALSE, warning=FALSE}

performing_listings <- performers(price_category)
```



```{r echo=FALSE, message=FALSE, warning=FALSE, fig.width=15, fig.height=10}
performers_table(performing_listings)
```



```{r echo=FALSE, message=FALSE, warning=FALSE, fig.width=12, fig.height=6}
performers_plot(performing_listings, price_category)
      
```


### Luxury price category
```{r echo=FALSE, message=FALSE, warning=FALSE}

price_category <-  'luxury'
```


```{r echo=FALSE, message=FALSE, warning=FALSE}

performing_listings <- performers(price_category)
```



```{r echo=FALSE, message=FALSE, warning=FALSE, fig.width=15, fig.height=10}
performers_table(performing_listings)
```



```{r echo=FALSE, message=FALSE, warning=FALSE, fig.width=12, fig.height=6}
performers_plot(performing_listings, price_category)
      
```


## Occupancy Heatmap




```{r  echo=FALSE, message=FALSE, warning=FALSE,}
calendar_df$price.local_price <- as.Date(calendar_df$price.date)
calendar_df <- prepare_time_series(calendar_df, 'price.date')
```

```{r  echo=FALSE, message=FALSE, warning=FALSE,}
summary_df <- calendar_df %>% 
          mutate(daily_revenue=daily_revenue(available, price.local_price)) %>% 
          group_by_(.dots=c('year', 'month', 'day','monthf','weekdayf','monthweek')) %>% 
          summarize(occupancy=occupancy(available))
summary_df$occupancy = summary_df$occupancy * 100
```



```{r echo=FALSE, message=FALSE, warning=FALSE,fig.height=4, fig.width=20}

heatmap(summary_df, summary_df$occupancy,  'Occupancy', 'Occupancy in %', c('#fc8d59', '#ffffbf', '#91cf60'))
```





