# -*- coding: utf-8 -*-
import urllib2
import urllib
import random
from bson import json_util
import json
import requests


class Airbnb:
    __API_CALENDAR_URL = 'https://www.airbnb.com/api/v2/calendar_months?key={4}&currency={6}&locale={5}&listing_id={0}&year={2}&month={1}&count={3}&_format=with_conditions'
    __API_SEARCH_LOCATION_URL = 'https://www.airbnb.com/api/v2/explore_tabs?_format=for_explore_search_web&experiences_per_grid=20&experiences_per_grid=20&timezone_offset=120&metadata_only=false&is_standard_search=true&refinement_paths[]=/home&selected_tab_id=home_tab&allow_override[]=&search_by_map=true&ne_lat={3}&ne_lng={4}&sw_lat={5}&sw_lng={6}&items_per_grid={7}&section_offset={8}&items_offset={8}&key={0}&currency={2}&locale={1}'
    __API_SEND_MESSAGE_URL = 'https://api.airbnb.com/v1/threads/create'
    __API_MESSAGES_URL = 'https://api.airbnb.com/v1/threads'
    __API_ACCOUNT_ACTIVE = 'https://api.airbnb.com/v1/account/active'
    __API_SEARCH_LOCATION_AND_CITY_URL = __API_SEARCH_LOCATION_URL + \
        '&location={7}'
    __API_LISTING_URL = 'https://api.airbnb.com/v2/listings/{0}?client_id={1}&_format=v1_legacy_for_p3&currency={3}&locale={2}'
    __GIMME_PROXY_API_URL = 'http://gimmeproxy.com/api/getProxy?anonymityLevel=1&supportsHttps=true&minSpeed=10&={0}'
    __access_token = None

    def __init__(self, api_key, client_id, proxies, username=None, password=None):
        self.client_id = client_id
        self.api_key = api_key
        self.proxies = proxies
        self.username = username
        self.password = password
        if username != None and password != None:
            self.__login()

    def __login(self):
        proxies = self.proxies.get()
        session = requests.Session()
        session.headers = {
            "Accept": "application/json",
            "Accept-Encoding": "gzip, deflate",
            "Content-Type": "application/json",
            "X-Airbnb-API-Key": self.api_key,
            "User-Agent": "Airbnb/4.7.0 iPhone/8.1.2"
        }
        login_payload = {"username": self.username,
                         "password": self.password,
                         "prevent_account_creation": "true"}

        r = session.post(self.__API_LOGIN_URL, data=json.dumps(
            login_payload), proxies=proxies)

        if "access_token" not in r.json():
            raise Exception('No access token')

        self.__access_token = r.json()["access_token"]

    def __request(self, url, headers, data):
        proxies = self.proxies.get()

        proxy = urllib2.ProxyHandler(
            proxies)
        opener = urllib2.build_opener(proxy)
        urllib2.install_opener(opener)

        req = urllib2.Request(url, headers=headers, data=data)

        return req

    def __get_json(self, url, data=None):
        header = {
            'User-Agent': "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1"}
        if self.__access_token:
            header["X-Airbnb-OAuth-Token"] = self.__access_token

        req = self.__request(url, header, data)

        try:
            response = urllib2.urlopen(req)

            status_code = response.getcode()

            if status_code == 200:
                return json.load(response)
            elif status_code != 401 and status_code != 403:
                return self.__get_json(url)
        except Exception as e:
            raise e

    def get_listings_for_location(self, ne_lat, ne_lng, sw_lat, sw_lng, num, offset=0, locale='en', currency='USD'):
        hostURL = self.__API_SEARCH_LOCATION_URL.format(
            self.api_key, locale, currency, ne_lat, ne_lng, sw_lat, sw_lng, num, offset)
        return self.__get_json(hostURL)

    def get_calendar(self, listing_id, start_month, start_year, months, locale='en', currency='USD'):
        hostURL = self.__API_CALENDAR_URL.format(
            listing_id, start_month, start_year, months, self.api_key, locale, currency)
        return self.__get_json(hostURL)

    def get_listing(self, listing_id, locale='en', currency='USD'):
        hostURL = self.__API_LISTING_URL.format(
            listing_id, self.client_id, locale, currency)
        return self.__get_json(hostURL)

    def get_listing(self, listing_id, locale='en', currency='USD'):
        hostURL = self.__API_LISTING_URL.format(
            listing_id, self.client_id, locale, currency)
        return self.__get_json(hostURL)

    def send_message(self, listing_id, checkin, checkout, number_of_guests,
                     message,  locale='en', currency='USD'):

        proxies = self.proxies.get()

        session = requests.Session()
        session.headers = {
            "Accept": "application/json",
            "Accept-Encoding": "gzip, deflate",
            "Content-Type": "application/json",
            "X-Airbnb-API-Key": self.api_key,
            "User-Agent": "Airbnb/4.7.0 iPhone/8.1.2",
            "X-Airbnb-OAuth-Token": self.__access_token
        }
        payload = {"message": message,
                   "checkout_date": checkout.strftime('%Y-%m-%dT%H:%M:%S'),
                   "checkin_date": checkin.strftime('%Y-%m-%dT%H:%M:%S'),
                   "number_of_guests": number_of_guests,
                   "listing_id": listing_id
                   }

        r = session.post(self.__API_SEND_MESSAGE_URL, data=json.dumps(
            payload, default=json_util.default), proxies=proxies)

        return r.json()

    def messages(self):
        hostURL = self.__API_MESSAGES_URL
        return self.__get_json(hostURL)

    def get_user_info(self):
        hostURL = self.__API_ACCOUNT_ACTIVE
        return self.__get_json(hostURL)
