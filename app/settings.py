import os

DEBUG = True

MONGO_HOST = os.getenv('MONGO_HOST', 'mongodb')
MONGO_PORT = os.getenv('MONGO_PORT', 27017)
MONGO_DBNAME = 'airbnb'


listings = {
    'schema': {
        'name': {
            'type': 'string',
            'minlength': 1,
            'maxlength': 20,
            'required': True,
        },
        'email': {
            'type': 'string',
            'minlength': 1,
            'maxlength': 20,
        },
        'airbnb_id': {
            'type': 'integer',
            'required': True,
            'unique': True,
        },
    },
    'resource_methods': ['GET', 'POST']
}

reports = {
    'schema': {
        'report': {
            'type': 'media'
        },
        'listing_id': {
            'type': 'string',
            'required': True
        },
    },
    'resource_methods': ['GET', 'POST']
}

DOMAIN = {'listings': listings, 'reports': reports}

RETURN_MEDIA_AS_BASE64_STRING = False
RETURN_MEDIA_AS_URL = True
EXTENDED_MEDIA_INFO = ['content_type', 'name', 'length']