# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import pymongo as mongo
import datetime
import json
import os.path as path
import os
import fileinput
import math
import csv


IMPORT_DIR = 'input/bookingCom/'
IMPORT_HOTEL_DIR = IMPORT_DIR + "hotels"
IMPORT_HOTEL_CITIES_DIR = IMPORT_DIR + "hotelcities"

MONGO_URL = 'mongodb://mongodb'


def __remove_row_with_no_ufi_id(row):
    try:
        int(row.ufi)
        int(row['id'])
        return row
    except:
        return {}


def __create_loc(row):
    try:
        lat = np.float64(row['latitude'])
        lon = np.float64(row['longitude'])
        return [lat, lon]
    except:
        return [np.float64(0), np.float64(0)]


def do_import():
    hotels = mongo.MongoClient(MONGO_URL).hotels.hotels
    hotels.drop()

    for filename in os.listdir(IMPORT_HOTEL_DIR):
        print "processing {}".format(filename)
        with open('{0}/{1}'.format(IMPORT_HOTEL_DIR, filename), 'rb') as f:
            hotels_df = pd.read_csv('{0}/{1}'.format(IMPORT_HOTEL_DIR, filename),
                                    sep='\t', error_bad_lines=False, encoding='utf-8')
            hotels_df.drop(['Unnamed: 38'], axis=1, inplace=True)
            hotels_df.apply(__remove_row_with_no_ufi_id, axis=1)
            hotels_df.dropna(subset=['id'], inplace=True)
            hotels_df.dropna(subset=['ufi'], inplace=True)
            hotels_df['loc'] = hotels_df.apply(__create_loc, axis=1)
            hotels_df.drop(['latitude', 'longitude'], axis=1, inplace=True)

            keys = hotels_df.columns.values

            hotels_df[['ufi']] = hotels_df[
                ['ufi']].astype(int, raise_on_error=False)
            records = json.loads(hotels_df.T.to_json()).values()

            try:
                hotels.insert_many(records)
            except Exception as e:
                print 'An error occurred {}'.format(e)

    hotels.delete_many({"loc.0": {'$not': {'$type': 1}}})
    hotels.create_index([("cc1", mongo.ASCENDING),
                         ("ufi", mongo.ASCENDING),
                         ("loc", mongo.ASCENDING)])
    hotels.create_index([("ufi", mongo.ASCENDING)])
    hotels.create_index([("loc", mongo.GEOSPHERE)])


if __name__ == "__main__":
    do_import()
