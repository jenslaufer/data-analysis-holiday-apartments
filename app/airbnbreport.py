#!/usr/local/bin/python
# -*- coding: utf-8 -*-

import argparse
import os
import pymongo as mongo
from jinja2 import Template
import json
from bson import json_util
import pandas as pd
import zipfile
import shutil
import glob
import ntpath
import random
import string
import boto3


if __name__ == "__main__":

    import ssl

    ssl._create_default_https_context = ssl._create_unverified_context

    parser = argparse.ArgumentParser(
        description='Creates Airbnb Report')
    parser.add_argument('-u', '--uri')
    parser.add_argument('-n', '--name')
    parser.add_argument('-t', '--target')
    parser.add_argument('-r', '--radius')
    parser.add_argument('-p', '--template')
    parser.add_argument('-c', '--currency')
    parser.add_argument('-q', '--query')
    parser.add_argument('-m', '--numberOfMonths')
    parser.add_argument('-db', '--dbUri')
    parser.add_argument('--details', action='store_true')

    parser.add_argument('--reportOnly', action='store_true')
    parser.add_argument('--reportForAll', action='store_true')
    parser.add_argument('--messageToListing', action='store_true')

    args = parser.parse_args()

    if args.uri == None:
        raise "A uri for Airbnb listing is needed"

    is_listing_details = args.details

    uri = args.uri
    name = args.name
    is_report_only = args.reportOnly
    is_report_for_all = args.reportForAll
    is_message_to_listing = args.messageToListing

    cwd = os.getcwd()

    docType = 'all'
    radius = 10000
    render_tmpl = "template/templ-report.rmd"
    currency = "USD"
    target = "temp/"
    number_of_months = 6
    mongourl = 'mongodb://mongodb'
    name = ''
    query = '"{}"'

    if args.numberOfMonths != None:
        number_of_months = int(args.numberOfMonths)
    if args.radius != None:
        radius = int(args.radius)
    if args.template != None:
        render_tmpl = args.template
    if args.currency != None:
        currency = args.currency
    if args.dbUri != None:
        mongourl = args.dbUri
    if args.target != None:
        target = args.target
    if args.name != None:
        name = args.name
    if args.query != None:
        query = args.query

    id = uri[uri.rfind('/') + 1:]

    details = ""
    if is_listing_details:
        details = "--details"
    exe = "python airbnb_import.py all -i {0} -t {1} -c {2} -m {3} -r {6} -db {4} {5}".format(
        id, target, currency, number_of_months, mongourl, details, radius)
    if not is_report_only:
        os.system(exe)

    mongo_client = mongo.MongoClient(mongourl)
    lst_coll = mongo_client.airbnb.listings

    root_listing = lst_coll.find_one({'id': int(id)})
    lat = root_listing['loc']['coordinates'][1]
    lng = root_listing['loc']['coordinates'][0]

    if not is_report_for_all:
        listings = [{"id": id, "lat": lat, "lng": lng}]
    else:
        listings = list(lst_coll.aggregate([
            {
                '$geoNear':
                    {
                        'near':
                        {
                            'type': 'Point',
                            'coordinates': [lng, lat]
                        },
                        'spherical': True,
                        'query': {},
                        'maxDistance': radius,
                        'distanceField': 'dist',
                        'limit': 10000
                    },
            },
            {
                '$project':
                    {
                        'id': '$id',
                        'lat': {'$arrayElemAt': ['$loc.coordinates', 0]},
                        'lng': {'$arrayElemAt': ['$loc.coordinates', 1]}
                    }
            }
        ]))

    for listing in listings:
        outputDir = '{}/reports/{}'.format(target, listing['id'])
        if not os.path.exists(outputDir):
            os.makedirs(outputDir)

        knitr_templ = """
        library('rmarkdown')

        setwd('{{cwd}}')
        render('{{target}}reports/{{id}}/report.rmd','{{docType}}', quiet=FALSE, params = list(
                mongourl='{{mongourl}}',
                name='{{name}}', 
                lat={{lat}},
                lng={{lng}}, 
                radius={{radius}}, 
                query={{query}},
                currency='{{currency}}', 
                root.listing.id={{root_listing_id}},
                number.of.months={{number_of_months}}
              ))
        """
        with open('{}/reports/{}/render.r'.format(target, listing['id']), 'w') as file:
            file.write(Template(knitr_templ).render(mongourl=mongourl,
                                                    target=target, id=listing['id'], docType=docType, name=name, lat=listing['lat'],
                                                    lng=listing[
                                                        'lng'], radius=radius, currency=currency,
                                                    root_listing_id=listing['id'], number_of_months=number_of_months, cwd=cwd, query=query))

        reports_dir = '{}/reports/{}'.format(target, listing['id'])

        shutil.copy2(render_tmpl, '{}/{}'.format(reports_dir, "report.rmd"))

        include_dir = '{}/include'.format(reports_dir)
        if os.path.exists(include_dir):
            shutil.rmtree(include_dir)

        shutil.copytree('template/include', include_dir)

        unique_key = ''.join(random.SystemRandom().choice(
            string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(6))

        os.system(
            'Rscript  {}/reports/{}/render.r'.format(target, listing['id']))

        with zipfile.ZipFile('{}/reports/{}/reports.zip'.format(target, listing['id']), 'w') as myzip:
            for name in glob.glob('{}/reports/{}/report.*'.format(target, listing['id'])):
                myzip.write(name, ntpath.basename(name))
