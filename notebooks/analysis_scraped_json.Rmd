---
title: "Untitled"
author: "Jens Laufer"
date: "15 August 2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.width = 30, fig.height = 15)
```

```{r}
library(tidyverse)
library(ndjson)
library(glue)
library(scales)
library(stringi)
```


```{r}
ID <- "96ced185-5780-5a2b-85ac-a0a581da3953"
DATA_DIR <- glue("../app/temp/{ID}/locations/output")
```



```{r}
all.listings <- tibble(id = numeric(), filename = character())
for(file in list.files(DATA_DIR)){
     listings <- stream_in(glue("{DATA_DIR}/{file}"))
     if(length(listings) > 0){
       listings <- listings %>% select(id) %>% mutate(filename=file) 
       all.listings <- bind_rows(all.listings, listings)
     }
}

all.listings <-  all.listings %>% mutate(center = str_extract(filename, "([0-9\\.\\-])+_([0-9\\.\\-])+")) 
```
`

```{r}
id_count <- (all.listings %>% distinct(id) %>% count())$n
file_count <- (all.listings %>% distinct(filename) %>% count())$n
center_count <- (all.listings %>% distinct(center) %>% count())$n
glue("{id_count} ids, {file_count} files, {center_count} centers")
```



```{r}
as.tibble(table(all.listings %>% select(id, filename))) %>% mutate(is.existing=ifelse(n > 0, T, F)) %>% 
   
    ggplot(aes(id, filename, fill=is.existing)) + 
    geom_raster(alpha=0.8) +
    scale_fill_manual(values=c('grey','black'))+
    theme(axis.text.x = element_text(angle = 90, hjust = 1)) 
    
    
```

```{r}
as.tibble(table(all.listings %>% select(id, center))) %>% mutate(is.existing=ifelse(n > 0, T, F)) %>% 
   
    ggplot(aes(id, center, fill=is.existing)) + 
    geom_raster(alpha=0.8) +
    scale_fill_manual(values=c('grey','black'))+
    theme(axis.text.x = element_text(angle = 90, hjust = 1)) 
    
    
```


