# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import pymongo as mongo
import math
import datetime
import calendar
import seaborn as sns
from ggplot import *
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import calmap


class HolidayApartmentInsights:

    def __init__(self, mongo_url):
        self.__mongo_url = mongo_url
        self.__calendar_dates = mongo.MongoClient(mongo_url).airbnb.dates
        self.__listings = mongo.MongoClient(mongo_url).airbnb.listings

    def __add_months(self, sourcedate, months):
        month = sourcedate.month - 1 + months
        year = int(sourcedate.year + month / 12)
        month = month % 12 + 1
        day = min(sourcedate.day, calendar.monthrange(year, month)[1])
        return datetime.datetime(year, month, day, 0, 0, 0)

    def get_listings(self, location, radius):
        query = {}
        result = list(self.__listings.aggregate(
            [
                {
                    '$geoNear':
                    {
                        'near':
                        {
                            'type': 'Point',
                            'coordinates': [location['lng'], location['lat']],
                        },
                        'spherical': True,
                        'query': query,
                        'maxDistance': radius,
                        'distanceField': 'dist',
                        'limit': 1000000
                    },
                }
            ]))
        return result

    def get_calendar_days_for_listing(self, listing_id, date_from=None, date_to=None):
        if date_from != None and date_to != None:
            query = {'id': listing_id, 'date': {'$gte': date_from},
                     '$and': [{"date": {'$lt': date_to}}]}
        elif date_from != None and date_to == None:
            query = {'id': listing_id, 'date': {'$gte': date_from}}
        elif date_from == None and date_to != None:
            query = {'id': listing_id, "date": {'$lt': date_to}}
        else:
            query = {'id': listing_id}

        result = list(self.__calendar_dates.aggregate(
            [
                {
                    '$match': query,
                },
                {
                    '$project':
                    {
                        'id': '$id',
                        'date': '$date',
                        'available': '$available',
                        'daily_rate': '$price.local_price'
                    }
                }
            ]))

        return result

    def get_calendar_days_for_location_listings(self, location, date_from=None, date_to=None, radius=2000):
        if date_from != None and date_to != None:
            query = {'date': {'$gte': date_from},
                     '$and': [{"date": {'$lt': date_to}}]}
        elif date_from != None and date_to == None:
            query = {'date': {'$gte': date_from}}
        elif date_from == None and date_to != None:
            query = {"date": {'$lt': date_to}}
        else:
            query = {}

        result = list(self.__calendar_dates.aggregate(
            [
                {
                    '$geoNear':
                    {
                        'near':
                        {
                            'type': 'Point',
                            'coordinates': [location['lng'], location['lat']]
                        },
                        'spherical': True,
                        'query': query,
                        'maxDistance': radius,
                        'distanceField': 'dist',
                        'limit': 1000000
                    },
                },
                {
                    '$project':
                    {
                        'id': '$id',
                        'date': '$date',
                        'available': '$available',
                        'daily_rate': '$price.local_price',
                        'revenue_per_night': {
                            "$cond": {
                                'if': {'$eq': ['$available', False]},
                                "then": '$price.local_price',
                                "else": 0
                            }
                        }
                    }
                }
            ]))
        listings = self.get_listings(location, radius)
        result = pd.DataFrame(result).merge(pd.DataFrame(listings)[['name',
                                                                    'beds', 'is_business_travel_ready', 'star_rating',
                                                                    'property_type', 'room_type', 'public_address',
                                                                    'person_capacity',
                                                                    'reviews_count', 'id', 'dist']], on='id')
        return result.to_dict()

    def calendar_summary(self, result):
        df = pd.DataFrame(result)
        occupancy = None
        number_of_listings = None
        daily_rate = None

        if len(df) > 0:
            number_of_available_days = float(
                len(df[df.available == True]))
            number_of_unavailable_days = float(
                len(df[df.available == False]))

            occupancy = number_of_unavailable_days / \
                (number_of_unavailable_days + number_of_available_days)
            number_of_listings = len(df.groupby('id'))
            daily_rate = df.drop(
                labels=['_id', 'id'], axis=1).describe().to_dict().values()[0]

        return (occupancy, number_of_listings, daily_rate)

    def __calculate_metrics(self, group):
        unavailable_days = group.available[group.available == 0].count()
        total_days = len(group.available)

        occupancy = round(float(unavailable_days) /
                          float(total_days) * float(100), 1)
        revenue_in_period = group.revenue_per_night.sum()
        if unavailable_days > 0:
            average_daily_rate = round(
                float(revenue_in_period) / float(unavailable_days), 2)
        else:
            average_daily_rate = float(0)

        revenue_per_available_room = round(
            float(revenue_in_period) / float(total_days), 2)

        revenue = 365 * revenue_per_available_room

        return pd.Series([revenue_in_period, revenue, occupancy, average_daily_rate, revenue_per_available_room],
                         index=['revenue_in_period', 'revenue_per_year', 'occupancy', 'average_daily_rate', 'revenue_per_available_room'])

    def summarize(self, grouping, calendar_df):
        return calendar_df.groupby(grouping).apply(self.__calculate_metrics).reset_index()

    ### Histogram ##
    def histogram_monthly_price(self, calendar_df, binwidth=1, xname='Number', yname='Distribution of price per night',
                                title=' ', currency=u'€'):
        return ggplot(aes(x='daily_rate'),
                      data=calendar_df) + geom_histogram(binwidth=binwidth) + xlab(xname) + ylab(yname + currency) + ggtitle(title)

    def histogram_occupancy(self, calendar_df, binwidth=1,
                            xname='Number', yname='Occupancy', title='Distribution of Occupancy'):
        summarized = self.summarize(['year', 'month', 'id'], calendar_df)
        return ggplot(aes(x='occupancy'),
                      data=summarized) + geom_histogram(binwidth=binwidth) + xlab(xname) + ylab(yname) + ggtitle(title)

    ### Overall ###

    def boxplot_occupancy(self, calendar_df, xname='All listings', yname='Occupancy', title='Overall Occupancy Over All Listings In Area', ax=None):
        summarized = self.summarize(['year', 'month', 'id'], calendar_df)

        p = summarized.boxplot('occupancy', grid=True, ax=ax)
        p.set_xlabel(xname)
        p.set_ylabel(yname)
        p.set_title(title)

        return p

    def boxplot_daily_rate(self, calendar_df, xname='All listings', yname='Daily Rate In ',
                           title='Overall Daily Rate Over All Listings In Area', currency=u'€', ax=None):

        p = calendar_df.boxplot('daily_rate', grid=True, ax=ax)
        p.set_xlabel(xname)
        p.set_ylabel(yname + currency)
        p.set_title(title)

        return p

    def boxplot_revenue(self, calendar_df, xname='All listings', yname='Revenue In ',
                        title='Overall Yearly Revenue Over All Listings In Area', currency=u'€', ax=None):
        summarized = self.summarize(['year', 'month', 'id'], calendar_df)

        p = summarized.boxplot('revenue_per_year', grid=True, ax=ax)
        p.set_xlabel(xname)
        p.set_ylabel(yname + currency)
        p.set_title(title)

        return p

    ### Listing ###
    def boxplot_occupancy_per_listing(self, calendar_df, xname='Airbnb Listing Id',
                                      yname='Occupancy', title='Occupancy Per Airbnb Listing Id', ax=None):
        summarized = self.summarize(['id', 'year', 'month'], calendar_df)

        p = summarized.boxplot('occupancy', by='id', grid=True, ax=ax)
        p.set_xlabel(xname)
        p.set_ylabel(yname)
        p.set_title(title)

        return p

    def boxplot_daily_rate_by_listing(self, calendar_df, xname='Airbnb Listing Id', yname='Daily Rate In ',
                                      title='Daily Rate Per Airbnb Listing Id', currency=u'€', ax=None):
        p = calendar_df.boxplot('daily_rate', by='id', grid=True, ax=ax)
        p.set_xlabel(xname)
        p.set_ylabel(yname + currency)
        p.set_title(title)

        return p

    def boxplot_revenue_by_listing(self, calendar_df, xname='Listing ID', yname='Revenue in ', currency=u'€',
                                   title='Revenue Per Month'):
        summarized = self.summarize(['id', 'year', 'month'], calendar_df)

        p = summarized.boxplot('revenue_per_year', by='id', grid=True, ax=ax)
        p.set_xlabel(xname)
        p.set_ylabel(yname + currency)
        p.set_title(title)

        return p

    ### Month ###
    def boxplot_occupancy_per_month(self, calendar_df, xname='Month Of The Year',
                                    yname='Occupancy', title='Occupancy Per Month Of The Year', ax=None):
        summarized = self.summarize(['year', 'month', 'id'], calendar_df)

        p = summarized.boxplot('occupancy', by='month', grid=True, ax=ax)
        p.set_xlabel(xname)
        p.set_ylabel(yname)
        p.set_title(title)

        return p

    def boxplot_daily_rate_per_month(self, calendar_df, xname='Month Of The Year', yname='Daily Rate in ',
                                     title='Daily Rate Per Month Of The Year', currency=u'€', ax=None):
        p = calendar_df.boxplot(
            'daily_rate', by='month', grid=True, ax=ax)
        p.set_xlabel(xname)
        p.set_ylabel(yname + currency)
        p.set_title(title)

        return p

    def boxplot_revenue_per_month(self, calendar_df, xname='Month', yname='Revenue In ',
                                  title='Revenue Per Month', currency=u'€', ax=None):
        summarized = self.summarize(['year', 'month', 'id'], calendar_df)

        p = summarized.boxplot(
            'revenue_in_period', by='month', grid=True, ax=ax)
        p.set_xlabel(xname)
        p.set_ylabel(yname + currency)
        p.set_title(title)

        return p

    ### Person capacity ###
    def boxplot_occupancy_per_person_capacity(self, calendar_df, xname='Person Capacity', yname='Occupancy',
                                              title='Occupancy Per Person Capacity', ax=None):
        summarized = self.summarize(['person_capacity', 'id'], calendar_df)

        p = summarized.boxplot(
            'occupancy', by='person_capacity', grid=True, ax=ax)
        p.set_xlabel(xname)
        p.set_ylabel(yname)
        p.set_title(title)

        return p

    def boxplot_daily_rate_per_person_capacity(self, calendar_df, xname='Person Capacity', yname='Daily Rate In ',
                                               title='Daily Rate Per Person Capacity', currency=u'€', ax=None):
        p = calendar_df.boxplot(
            'daily_rate', by='person_capacity', grid=True, ax=ax)
        p.set_xlabel(xname)
        p.set_ylabel(yname + currency)
        p.set_title(title)

        return p

    def boxplot_revenue_per_person_capacity(self, calendar_df, xname='Person Capacity', yname='Revenue in ',
                                            title='Yearly Revenue Per Person Capacity', currency=u'€', ax=None):
        summarized = self.summarize(
            ['person_capacity', 'month', 'id'], calendar_df)

        p = summarized.boxplot(
            'revenue_per_year', by='person_capacity', grid=True, ax=ax)
        p.set_xlabel(xname)
        p.set_ylabel(yname + currency)
        p.set_title(title)

        return p

    ### Property Type ###

    def boxplot_occupancy_per_property_type(self, calendar_df, xname='Property Type', yname='Occupancy',
                                            title='Occupancy Per Property Type', ax=None):
        summarized = self.summarize(['property_type', 'id'], calendar_df)

        p = summarized.boxplot(
            'occupancy', by='property_type', grid=True, ax=ax)
        p.set_xlabel(xname)
        p.set_ylabel(yname)
        p.set_title(title)

        return p

    def boxplot_daily_rate_per_property_type(self, calendar_df, xname='Property Type', yname='Daily Rate In ',
                                             title='Daily Rate Per Property Type', currency=u'€', ax=None):
        p = calendar_df.boxplot(
            'daily_rate', by='property_type', grid=True, ax=ax)
        p.set_xlabel(xname)
        p.set_ylabel(yname + currency)
        p.set_title(title)

        return p

    def boxplot_revenue_per_property_type(self, calendar_df, xname='Property Type', yname='Revenue In ',
                                          title='Yearly Revenue Per Property Type', currency=u'€', ax=None):
        summarized = self.summarize(
            ['property_type', 'month', 'id'], calendar_df)

        p = summarized.boxplot(
            'revenue_per_year', by='property_type', grid=True, ax=ax)
        p.set_xlabel(xname)
        p.set_ylabel(yname + currency)
        p.set_title(title)

        return p

    # occupancy heatmap
    def heatmap_occupancy(self, calendar_df):
        summarized = self.summarize(['date'], calendar_df)
        events = pd.Series(summarized.occupancy.values,
                           index=summarized.date.values)

        fig, axs = calmap.calendarplot(
            events, fillcolor='grey', cmap='RdYlGn', linewidth=4, fig_kws=dict(figsize=(20, 20)))

        colorbar = fig.colorbar(axs[0].get_children()[1], ax=axs.ravel(
        ).tolist(), orientation='horizontal', shrink=.4, aspect=20)
        colorbar.ax.tick_params(labelsize=15)
        for ax in axs:
            ax.tick_params(labelsize=20)

        return (fig, axs)

    def heatmap_daily_revenue(self, calendar_df):
        summarized = self.summarize(['date'], calendar_df)
        events = pd.Series(summarized.revenue_in_period.values,
                           index=summarized.date.values)

        fig, axs = calmap.calendarplot(
            events, fillcolor='grey', cmap='RdYlGn', linewidth=4, fig_kws=dict(figsize=(20, 20)))

        colorbar = fig.colorbar(axs[0].get_children()[1], ax=axs.ravel(
        ).tolist(), orientation='horizontal', shrink=.4, aspect=20)
        colorbar.ax.tick_params(labelsize=15)
        for ax in axs:
            ax.tick_params(labelsize=20)
            ax.set_title('Heatmap Revenue Of All Listings In Area Per Day')

        return (fig, axs)

    def heatmap_average_daily_rate(self, calendar_df):
        summarized = self.summarize(['date'], calendar_df)
        events = pd.Series(summarized.average_daily_rate.values,
                           index=summarized.date.values)

        fig, axs = calmap.calendarplot(
            events, fillcolor='grey', cmap='RdYlGn', linewidth=4, fig_kws=dict(figsize=(20, 20)))

        colorbar = fig.colorbar(axs[0].get_children()[1], ax=axs.ravel(
        ).tolist(), orientation='horizontal', shrink=.4, aspect=20)
        colorbar.ax.tick_params(labelsize=15)
        for ax in axs:
            ax.tick_params(labelsize=20)

        return (fig, axs)

    def heatmap_revenue_per_available_room(self, calendar_df):
        summarized = self.summarize(['date'], calendar_df)
        events = pd.Series(summarized.revenue_per_available_room.values,
                           index=summarized.date.values)

        fig, axs = calmap.calendarplot(
            events, fillcolor='grey', cmap='RdYlGn', linewidth=4, fig_kws=dict(figsize=(20, 20)))

        colorbar = fig.colorbar(axs[0].get_children()[1], ax=axs.ravel(
        ).tolist(), orientation='horizontal', shrink=.4, aspect=20)
        colorbar.ax.tick_params(labelsize=15)
        for ax in axs:
            ax.tick_params(labelsize=20)

        return (fig, axs)

    # Plot Overview

    def boxplots_occupancy(self, calendar_df, title='Occupancy'):
        fig = plt.figure()
        fig.set_figheight(15)
        fig.set_figwidth(15)

        ax0 = plt.subplot2grid((3, 3), (0, 0), colspan=2, rowspan=3)
        ax1 = plt.subplot2grid((3, 3), (0, 2), colspan=1)
        ax2 = plt.subplot2grid((3, 3), (1, 2), colspan=1)
        ax3 = plt.subplot2grid((3, 3), (2, 2), colspan=1)

        self.boxplot_occupancy(calendar_df, ax=ax0)
        self.boxplot_occupancy_per_person_capacity(
            calendar_df, ax=ax2)
        self.boxplot_occupancy_per_property_type(
            calendar_df, ax=ax3)
        self.boxplot_occupancy_per_month(
            calendar_df, ax=ax1)

        plt.suptitle(title)

    def boxplots_daily_rate(self, calendar_df, title='Daily Rate'):
        fig = plt.figure()
        fig.set_figheight(15)
        fig.set_figwidth(15)

        ax0 = plt.subplot2grid((3, 3), (0, 0), colspan=2, rowspan=3)
        ax1 = plt.subplot2grid((3, 3), (0, 2), colspan=1)
        ax2 = plt.subplot2grid((3, 3), (1, 2), colspan=1)
        ax3 = plt.subplot2grid((3, 3), (2, 2), colspan=1)

        self.boxplot_daily_rate(calendar_df, ax=ax0)
        self.boxplot_daily_rate_per_person_capacity(
            calendar_df, ax=ax2)
        self.boxplot_daily_rate_per_property_type(
            calendar_df, ax=ax3)
        self.boxplot_daily_rate_per_month(
            calendar_df, ax=ax1)

        plt.suptitle(title)

    def boxplots_revenue(self, calendar_df, title='Revenue'):
        fig = plt.figure()
        fig.set_figheight(15)
        fig.set_figwidth(15)

        ax0 = plt.subplot2grid((3, 3), (0, 0), colspan=2, rowspan=3)
        ax1 = plt.subplot2grid((3, 3), (0, 2), colspan=1)
        ax2 = plt.subplot2grid((3, 3), (1, 2), colspan=1)
        ax3 = plt.subplot2grid((3, 3), (2, 2), colspan=1)

        self.boxplot_revenue(calendar_df, ax=ax0)
        self.boxplot_revenue_per_person_capacity(
            calendar_df, ax=ax2)
        self.boxplot_revenue_per_property_type(
            calendar_df, ax=ax3)
        self.boxplot_revenue_per_month(
            calendar_df, ax=ax1)

        plt.suptitle(title)

    def bubble_plot(self, calendar_df, title="Average Daily Rate (ADR) vs Occupancy (Occ) vs Revenue",
                    xname="Average Daily Rate (ADR) in ", yname="Occupancy (Occ) in %", currency="EUR"):
        summarized = self.summarize(['name', 'property_type'], calendar_df)

        import matplotlib.pyplot as plt
        fig = plt.figure()
        fig.set_figheight(20)
        fig.set_figwidth(20)

        ax_ = plt.subplot2grid((1, 1), (0, 0))
        ax = summarized.plot.scatter(x='average_daily_rate', y='occupancy',
                                     s=summarized.revenue_per_year,
                                     c=summarized.revenue_per_year,
                                     cmap='RdYlGn',
                                     alpha=.7, ax=ax_, legend=True)
        ax.set_title(title, fontsize=30)
        ax.set_xlabel(xname + currency, fontsize=20)
        ax.set_ylabel(yname, fontsize=20)
        ax.tick_params(labelsize=15)

        for i, txt in enumerate(summarized.name):
            ax.annotate(txt,
                        (summarized.average_daily_rate.iat[
                            i], summarized.occupancy.iat[i]),
                        va='center', ha='center', fontsize=13)
        return ax
