FROM jenslaufer/python-r:py_2.7-r_3.5

ADD requirements.txt requirements.txt
ADD requirements.r requirements.r
ADD app/ app/

RUN echo "deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/3.6 main" | tee /etc/apt/sources.list.d/mongodb-org-3.6.list
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5

RUN apt-get update && apt-get -y upgrade

# For mongo commnds
RUN apt-get install -y mongodb-org-tools

RUN apt-get install -y libgdal-dev libproj-dev libgeos-dev
RUN apt-get install -y libxml2-dev libxslt-dev
RUN apt-get install -y wget
RUN apt-get install -y git

RUN wget https://github.com/jgm/pandoc/releases/download/2.2.2.1/pandoc-2.2.2.1-1-amd64.deb 
RUN dpkg -i pandoc-2.2.2.1-1-amd64.deb
RUN rm pandoc-2.2.2.1-1-amd64.deb

RUN pip install -r requirements.txt
RUN Rscript requirements.r

RUN apt-get update && apt-get -y upgrade

CMD ["/app/run.py"]